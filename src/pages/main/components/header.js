import React from "react";
import Icon from "../../../assets/iconChuck.png";
import Logo from "../styles/header";

function Header() {
  return (
    <Logo>
      <img src={Icon} alt="Chuck Icon" />
      <p>Chuck Norris Jokes</p>
    </Logo>
  );
}

export default Header;
