import React from "react";
import Info from "../styles/info";

function Informations() {
  return (
    <>
      <Info>
        <p>Who is Chuck Norris?</p>
        <p>
          Chuck Norris facts are satirical factoids about American martial
          artist and actor Chuck Norris that have become an Internet phenomenon
          and as a result have become widespread in popular culture. The 'facts'
          are normally absurd hyperbolic claims about Norris's toughness,
          attitude, sophistication, and masculinity.
        </p>
      </Info>
    </>
  );
}

export default Informations;
