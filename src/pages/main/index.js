import React, { useState, useEffect, useCallback } from "react";

import Header from "./components/header";
import Info from "./components/info";
import JokeBox from "./components/jokeBox";

import Api from "../../api";

function App() {
  const [Joke, setJoke] = useState("");
  const [limit, setLimit] = useState(0);

  const getRandomJoke = useCallback(() => {
    async function getRandomJoke() {
      const joke = await Api.get("/random?escape=javascript");
      setJoke(joke.data.value);
    }
    getRandomJoke();
  }, []);
  const makeOwnJoke = useCallback((name, lastname) => {
    async function ownJoke() {
      const joke = await Api.get(
        `/random?firstName=${name || "Chuck"}&lastName=${lastname || "Norris"}`
      );
      setJoke(joke.data.value);
    }
    ownJoke();
  }, []);
  const findJoke = useCallback(
    (id) => {
      async function findJoke() {
        if (Number(id) > limit || Number(id) < 0) {
          alert("Sorry we haven't created this joke yet");
          getRandomJoke();
        } else if (id === "") {
          getRandomJoke();
        } else {
          const joke = await Api.get(`/${id}`);
          if (!joke.data.value.id) {
            alert(
              `Sorry but apparently this joke no longer exists on our system! Shall we see another one? :)`
            );
            getRandomJoke();
          } else {
            setJoke(joke.data.value);
          }
        }
      }
      findJoke();
    },
    // eslint-disable-next-line
    [limit]
  );

  useEffect(() => {
    async function limitJokes() {
      const limitJoke = await Api.get("/");
      setLimit(limitJoke.data.value.length);
    }
    limitJokes();
    getRandomJoke();
    // eslint-disable-next-line
  }, []);

  return (
    <>
      <Header />
      <Info />
      <JokeBox
        jokeData={Joke}
        getRandomJoke={() => {
          getRandomJoke();
        }}
        makeOwnJoke={(name, lastname) => {
          makeOwnJoke(name, lastname);
        }}
        findJoke={(id) => {
          findJoke(id);
        }}
      ></JokeBox>
    </>
  );
}

export default App;
