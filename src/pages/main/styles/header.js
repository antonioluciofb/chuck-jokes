import styled from "styled-components";

const Header = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  img {
    margin-right: 10px;
    width: 160px;
    height: 160px;
  }

  p {
    font-size: 50px;
    font-family: "Jockey One", sans-serif;
    color: #373737;
    letter-spacing: 5px;
    align-items: center;
    justify-content: center;
  }
`;

export default Header;
