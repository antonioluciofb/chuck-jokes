import styled from "styled-components";

const Info = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;

  p {
    font-family: "Chakra Petch", sans-serif;
    margin-top: 10px;
    font-size: 24px;
    text-align: center;
    width: 60%;
    display: flex;
    justify-content: center;
  }
`;

export default Info;
